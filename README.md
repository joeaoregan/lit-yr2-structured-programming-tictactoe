```c
 
Tic Tac Toe

Player 1 = X
Player 2 = O

1 | 2 | 3
__________
4 | 5 | 6
__________
7 | 8 | 9
```

# TicTacToe - C
## Joe O'Regan
## K00203642
### Games Design and Development: Year 2
### Structured Programming

* 05/06/2018 Added coloured characters

![TicTacToe](https://raw.githubusercontent.com/joeaoregan/Yr2-TicTacToe/master/Screenshots/TicTacToe1.png "TicTacToe")

TicTacToe console game



---

![TicTacToe Coloured Text](https://raw.githubusercontent.com/joeaoregan/Yr2-TicTacToe/master/Screenshots/TicTacToe2.png "TicTacToe Coloured Text")

Added colour

---

![TicTacToe More Coloured Text](https://raw.githubusercontent.com/joeaoregan/Yr2-TicTacToe/master/Screenshots/TicTacToe3.png "TicTacToe More Coloured Text")

More coloured text

---